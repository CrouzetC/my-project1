package net.tncy.ccr.sandbox;

/**
 * Make a division
 */
public class Calculator
{
    public static int divide(int dividend, int divisor)
    {
        return dividend / divisor;
    }
}
