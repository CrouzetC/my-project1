package net.tncy.ccr.sandbox;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Test for Calculator
     */
    @Test
    public void testCalculator()
    {
        assertTrue(Calculator.divide(4, 2) == 2);
        assertEquals(Calculator.divide(3, 2), 1);
        assertEquals(Calculator.divide(0, 1), 0);
    }
}
